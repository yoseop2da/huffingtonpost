//
//  articleView.m
//  HuffingtonPost
//
//  Created by yoseop on 2014. 10. 16..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "ArticleView.h"
@interface ArticleView()

@property (strong) UILabel *label;
@property (strong) UIImageView *imageView;

@end

@implementation ArticleView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.userInteractionEnabled = YES;
#define imageViewWidth 110
#define imageViewHeight 110
        CGFloat imageViewOrigin = (frame.size.height - 110)/2;
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageViewOrigin, imageViewOrigin, imageViewWidth, imageViewHeight)];
        self.imageView.image = [UIImage imageNamed:@"back"];
        [self addSubview:self.imageView];
        
        
        CGFloat labelOriginX = self.imageView.frame.size.width + imageViewOrigin + imageViewOrigin;
        CGFloat labelOriginY = imageViewOrigin;
        CGFloat labelWidth = [UIScreen mainScreen].bounds.size.width - labelOriginX - labelOriginY;
        CGFloat labelHeight = imageViewHeight;
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(labelOriginX, labelOriginY, labelWidth, labelHeight)];
        self.label.numberOfLines = 0;
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.textColor = [UIColor blackColor];
        self.label.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.label];
        
        self.backgroundColor = [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:0.8f];
    }
    return self;
}

- (void)setData:(NSDictionary *)data
{
    _data = data;
    
    self.label.text = data[@"title"];
    self.imageView.image = [UIImage imageNamed:data[@"image"]];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    NSLog(@"222222.touches : %@",self.data);
    [self.delegate didSelectedArticleViewWithData:self.data];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
}

@end
