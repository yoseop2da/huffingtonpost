//
//  ViewController.h
//  HuffingtonPost
//
//  Created by yoseop on 2014. 10. 16..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

- (void)didSelectedArticleViewWithData:(id)data;

@end

