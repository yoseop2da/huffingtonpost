//
//  ArticleSubViewContorller.m
//  HuffingtonPost
//
//  Created by yoseop on 2014. 10. 17..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "ArticleSubViewContorller.h"

@interface ArticleSubViewContorller ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ArticleSubViewContorller

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = self.data[@"title"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeButtonTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
