//
//  FirstArticleView.m
//  HuffingtonPost
//
//  Created by yoseop on 2014. 10. 17..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "FirstArticleView.h"
@interface FirstArticleView()

@property (strong) UILabel *label;
@property (strong) UIImageView *imageView;

@end

@implementation FirstArticleView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.userInteractionEnabled = YES;

        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 120, screenWidth, 120)];
        self.label.numberOfLines = 0;
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.font = [UIFont boldSystemFontOfSize:40];
        self.label.textColor = [UIColor whiteColor];
        self.label.backgroundColor = [UIColor clearColor];
        [self addSubview:self.label];
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setData:(NSDictionary *)data
{
    _data = data;
    
    self.label.text = data[@"title"];
    self.imageView.image = [UIImage imageNamed:data[@"image"]];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    NSLog(@"222222.touches : %@",self.data);
    [self.delegate didSelectedArticleViewWithData:self.data];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
}

@end

