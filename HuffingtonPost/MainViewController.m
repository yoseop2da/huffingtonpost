//
//  ViewController.m
//  HuffingtonPost
//
//  Created by yoseop on 2014. 10. 16..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "MainViewController.h"

#import "FirstArticleView.h"
#import "ArticleView.h"
#import "ArticleSubViewContorller.h"

@interface MainViewController ()<UIScrollViewDelegate>
@property (strong) NSArray *allData;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@end

@implementation MainViewController

#define screenWidth [UIScreen mainScreen].bounds.size.width
#define screenHight [UIScreen mainScreen].bounds.size.height
#define defaultArticleHight 120

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"프론트페이지";

    [self loadData];
    [self setScrollViewFrame];
    [self setContentViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setScrollViewFrame
{
    CGRect frame = CGRectMake(0, 0, screenWidth, screenHight);
    self.scrollView.frame = frame;
    self.scrollView.contentSize = CGSizeMake(screenWidth, defaultArticleHight * (self.allData.count + 1));
}

- (void)setContentViews
{
    [self makeChildView];
}

- (void)makeChildView
{
    int index = 0;

    for (NSDictionary *data in self.allData) {
        
        if (index == 0) {
            
            CGRect frame = CGRectMake(0,defaultArticleHight * index, screenWidth, defaultArticleHight * 2);
            
            FirstArticleView *view = [[FirstArticleView alloc] initWithFrame:frame];
            view.data = data;
            view.delegate = self;
            [self.scrollView addSubview:view];
            self.backGroundImageView.image = [UIImage imageNamed:data[@"image"]];
            
        }else{

            CGRect frame = CGRectMake(0,defaultArticleHight * (index+1), screenWidth, defaultArticleHight);
            ArticleView *view = [[ArticleView alloc] initWithFrame:frame];
            view.data = data;
            view.delegate = self;
//            view.label.text = data[@"title"];

            [self.scrollView addSubview:view];
        }

        index++;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
#define navigationArea 65
    CGFloat y = navigationArea - (scrollView.contentOffset.y/2);
//    CGFloat y = (scrollView.contentOffset.y/2);
    // 스크롤 할 때 하단의 이미지뷰의 이미지를 이동시키기
    self.backGroundImageView.frame = CGRectMake(0, y, self.backGroundImageView.frame.size.width, self.backGroundImageView.frame.size.height);
}


- (void)loadData
{
    NSDictionary *dataDic1 = @{@"title": @"풍부하게 같은 천지는 있는 얼마나 끓는다.", @"image": @"img1"};
    NSDictionary *dataDic2 = @{@"title": @"장식하는 인간에 그림자는 얼마나 그리하였는가? ", @"image": @"img2"};
    NSDictionary *dataDic3 = @{@"title": @"청춘의 곳이 이상의 옷을 미인을 끝에 듣기만 피다.", @"image": @"img3"};
    NSDictionary *dataDic4 = @{@"title": @"풀이 인생을 때문이다. 우리 이상을 보이는 청춘의 있으며,,,", @"image": @"img4"};
    NSDictionary *dataDic5 = @{@"title": @"청춘의 원질이 원대하고, 것이다. 같으며,,,,", @"image": @"img5"};
    NSDictionary *dataDic6 = @{@"title": @"살 인생에 현저하게 품에 곳이 청춘의 그들은 위하여서...", @"image": @"img6"};
    
    self.allData = @[dataDic1,dataDic2,dataDic3,dataDic4,dataDic5,dataDic6];
}

- (void)didSelectedArticleViewWithData:(NSDictionary *)data
{
    UINavigationController *articleNavigationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ArticleNavigationViewController"];
    
    ArticleSubViewContorller *articleSubViewContorller = articleNavigationViewController.childViewControllers.firstObject;
    articleSubViewContorller.data = data;
    
    [self presentViewController:articleNavigationViewController animated:YES completion:nil];
}

@end
