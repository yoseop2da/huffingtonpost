//
//  articleView.h
//  HuffingtonPost
//
//  Created by yoseop on 2014. 10. 16..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface ArticleView : UIView

@property (strong) MainViewController *delegate;
@property (strong,nonatomic) NSDictionary *data;

@end
